import time
import argparse
X = 0
Y = 1


def print_field(field):
    """Функция красиво выводит матрицу на экран
    :param field матрица:
    """
    for j in field[::-1]:
        print(j)


def move(start, field):
    """Функция просчитывает возможные ходы коня

    Для каждого возможного хода с начально точки
    функция проверяет, возможен ли этот ход в
    данных условиях, затем реализовывает его

    :param start: кортеж с координатами начальной точки
    :param field: матрица, которая служит игровым полем
    :return: начальная матрица, но уже с просчитанными ходами
    """
    if 0 <= start[1]+2 < 8 and 0 <= start[0]+1 < 8:
        field[start[1]+2][start[0]+1] = field[start[1]][start[0]]+1
    if 0 <= start[1]+1 < 8 and 0 <= start[0]+2 < 8:
        field[start[1]+1][start[0]+2] = field[start[1]][start[0]]+1
    if 0 <= start[1]-1 < 8 and 0 <= start[0]+2 < 8:
        field[start[1]-1][start[0]+2] = field[start[1]][start[0]]+1
    if 0 <= start[1]-2 < 8 and 0 <= start[0]+1 < 8:
        field[start[1]-2][start[0]+1] = field[start[1]][start[0]]+1
    if 0 <= start[1]-2 < 8 and 0 <= start[0]-1 < 8:
        field[start[1]-2][start[0]-1] = field[start[1]][start[0]]+1
    if 0 <= start[1]-1 < 8 and 0 <= start[0]-2 < 8:
        field[start[1]-1][start[0]-2] = field[start[1]][start[0]]+1
    if 0 <= start[1]+1 < 8 and 0 <= start[0]-2 < 8:
        field[start[1]+1][start[0]-2] = field[start[1]][start[0]]+1
    if 0 <= start[1]+2 < 8 and 0 <= start[0]-1 < 8:
        field[start[1]+2][start[0]-1] = field[start[1]][start[0]]+1
    return field


def moves(steps, field):
    """Функция ищет точки (адреса матрицы) с которых нужно начать ход,
    затем запускает подпрограмму 'move', которая реализует просчет ходов
    с этих точек
    :param steps: кол-во проделанных шагов
    :param field: матрица (игровое поле)
    :return: начальная матрица, но уже с просчитанными ходами
    """
    for j in range(8):
        for i in range(8):
            if field[j][i] == steps:
                field = move((i, j), field)
    return field


def knight_move(start, finish):
    '''функция по просчету нужного кол-ва ходов коню,
    для перемещения в конечную точку из начальной

    Алгоритм просчитывает ВСЕ возможные алгоритмы ходов коня из
    заданной клетки, пока не появится ход, подразумевающий попадания
    в нужную клетку
    :param a: Координаты начальной клетки
    :param finish: Координаты конечной клетки
    :return: кол-во шагов, необходимое, для попадания в нужную клетку
    '''
    field = []
    for _ in range(8):
        row = []
        for _ in range(8):
            row.append(256)
        field.append(row)
    field[start[1]-1][start[0]-1] = 0
    steps = 0
    while True:
        field = moves(steps, field)
        steps += 1
        if field[finish[1]-1][finish[0]-1] != 256:
            return field[finish[1]-1][finish[0]-1]


def knights_collision(start, finish):
    '''Перенаправляет запрос в функцию 'knight_move',
    производит с результатом некоторые манимуляции,
    получает и выводит кол-во ходов, за которое два коня могут встретиться
    :param start: стартовая клетка
    :param finish: финишная клетка
    :return: кол-во ходов, за которое два коня могут встретиться
    '''
    collision = knight_move(start, finish)
    return collision/2


def main():
    '''Накопление аргументов из коммандной строки,
    необходимых для работы программы, консольный интерфейс
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--start', nargs='+', type=int)
    parser.add_argument('-f', '--finish', nargs='+', type=int)
    parser.add_argument('-fi', '--first', nargs='+', type=int)
    parser.add_argument('-se', '--second', nargs='+', type=int)
    args = parser.parse_args()
    start = args.start
    finish = args.finish
    first = args.first
    second = args.second
    if not ((start and finish) or (first and second)):
        print("Для работы программы хотя бы start и finish или first и second должны быть введены")
    elif any([start,finish,first,second])<0 or any([start,finish,first,second])>9:
        print('Принимаются значения от нуля до восьми')
    elif start and finish:
        if start==finish:
            print('точки не могут совпадать')
        else:
            print(knight_move(start, finish))
    else:
        if first==second:
            print('точки не могут совпадать')
        else:
            print(knights_collision(first, second))


if __name__ == '__main__':
    main()
